//При помощи прототипного наследования мы можем получать значения и методы для создания объеков на основе предидущих классов
//и таким образом расширять новые объекты с помощью новых шаблонов классов, но при этом наследуя свойства предидущих.

//super() в конструкторе нового класса используется для передачи копии получаемых аргументов в новый класс из предидущего.

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    getName() {
        return this.name;
    }

    getAge() {
        return this.age;
    }

    getSalary() {
        return this.salary;
    }

    setName(value) {
        this.name = value;
    }

    setAge(value) {
        this.age = value;
    }

    setSalary(value) {
        this.salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    setSalary(value) {
        this.salary = value * 3;
    }
}

const programmer1 = new Programmer('Vlad', 23, 1000, 'JS')
const programmer2 = new Programmer('Andrey', 26, 1200, 'Java')
const programmer3 = new Programmer('Evgeniy', 28, 1400, 'C++')

programmer1.setSalary(2000)

console.log(programmer1, programmer2, programmer3)



